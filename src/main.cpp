
#include <Wire.h>
#include <Arduino.h>
#include <math.h>

#define  R0_value 10000.0
#define VCC 5.0
#define DEADZONE 0.03



const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9;  // Analog output pin that the LED is attached to
int sensorValue = 0;        // value read from the pot
int outputValue = 0;              // value output to the PWM (analog out)
float lux = 0.0;
float sensorResistance;
float resMedian =0;
float req=0.0;
float tau = 32.536;
float vf = 0.0;
float x_adj = 0.0;
float x_des = 20.0;
float o_ext = 0.0;
unsigned long prevTime =0;
unsigned long sampInterval = 10000;
unsigned long sampTime = 10;
float Kp = 10.0;
float Ki = 0.1;
float K1 = 0.0;
float K2 = 0.0;
float b = 1;
float a = 0.0;
float e =0;
float p =0;
float i =0;
float u = 0;
float y_ant = 0.0;
float i_ant = 0.0;
float e_ant = 0.0;


float gain;
float gain_ajust_lux[5];
float dc_gain;
float get_lux1(float sResistance);
float get_lux2(float sResistance);
void set_static_gain();
float pid_feedback(float y,float ref);
float lux_to_volts1(float lux);
void pid_setup();

void setup() {
  Serial.begin(9600);            // initialize serial communications at 9600 bps
  set_static_gain();
  x_adj= (x_des-dc_gain)/(gain);
  pid_setup();
}

void loop() {

  unsigned long startTime = micros();
  sensorValue = analogRead(analogInPin);

                   // read the analog in value//
  sensorResistance =  (VCC - (sensorValue * (5.0/1023.0))) / (sensorValue * (5.0/1023.0) / R0_value);
  req = (R0_value*sensorResistance)/(R0_value+sensorResistance);
  lux = get_lux1(sensorResistance);
  u = pid_feedback((sensorValue * (5.0/1023.0)),lux_to_volts1(x_des));


  //analogWrite(analogOutPin,(u+x_adj));
  //Tau calc
  Serial.print(lux);
  Serial.print(" ");
  Serial.print(u);
  Serial.print(" ");
  Serial.print(e);
  Serial.print(" ");
  Serial.print(lux_to_volts1(x_des));
  Serial.print(" ");
  Serial.print((sensorValue * (5.0/1023.0)));
  Serial.print(" ");
  Serial.println(startTime);
  analogWrite(analogOutPin, u+x_adj);

  //if((sensorValue * (5.0/1023.0)) >2.8) while(1);

  /*
  Serial.print(sensorResistance);
  Serial.print("\t");
  Serial.print(lux);
  Serial.print("\t");
  Serial.print(gain);
  Serial.print("\t");
  Serial.println(outputValue);
  */

  unsigned long endTime = micros();
  delayMicroseconds(sampInterval - (endTime - startTime));
  //delay(250);

}


void set_static_gain(){
    Serial.println("GAIN CALIBRATION SEQUENCE");
    sensorValue = analogRead(analogInPin);                  // read the analog in value//
    sensorResistance =  (VCC - (sensorValue * (5.0/1023.0))) / (sensorValue * (5.0/1023.0) / R0_value);
    dc_gain=get_lux1(sensorResistance);

    Serial.print("LUX0: ");
    Serial.println(dc_gain);
    for(int i=0; i<5;i++){
      analogWrite(analogOutPin, 255.0*((i+1)/5.0));
      delay(1000);
      sensorValue = analogRead(analogInPin);                  // read the analog in value//
      sensorResistance =  (VCC - (sensorValue * (5.0/1023.0))) / (sensorValue * (5.0/1023.0) / R0_value);
      gain_ajust_lux[i]= get_lux1(sensorResistance) - dc_gain;
      Serial.print("LUX-LUX0: ");
      Serial.print(gain_ajust_lux[i]);
      float division = 5.0*(i+1)/5.0;
      Serial.print("\t");
      Serial.print(division);
      gain_ajust_lux[i]=gain_ajust_lux[i]/division;
      Serial.print("\t");
      Serial.println(gain_ajust_lux[i]);
      Serial.print("\t");
      Serial.println(sensorValue * (5.0/1023.0));
    }
    for (size_t i = 0; i < 5; i++) {

      gain+=gain_ajust_lux[i];
    }
    gain= gain/ 5.0;
  //  Serial.print("Real Gain= ");
  //  Serial.println(gain);
  //  Serial.println();
    analogWrite(analogOutPin, 0);
    delay(100);
}

float get_lux1(float sResistance){

  float aux = ((log10(sResistance)- 4.858) / (-0.786)); //aux = ((log10(sensorResistance[n_medidas-1])- 4.858) / (-0.786)); SENSOR 1

  return pow(10,aux);
}

float get_lux2(float sResistance){

  float aux = ((log10(sResistance)- 4.06106) / (-0.30421)); //aux = ((log10(sensorResistance[n_medidas-1])- 4.858) / (-0.786)); SENSOR 1

  return pow(10,aux);
}

void pid_setup(){
  K1 = Kp*b;
  K2 = Kp*Ki*sampTime/2;

  y_ant= 0; i_ant= 0; e_ant= 0;
}

float pid_feedback(float y,float ref){

  e = ref - y ;
  if(abs(e)<DEADZONE) e= 0;
  p = Kp * e;
  i = i_ant+ K2*(e + e_ant);

  y_ant= y;
  i_ant= i;
  e_ant= e;
  return p+i;
}

float lux_to_volts1(float lux){
  float aux = (log10(lux)*(-0.786)+ 4.858);
  aux =pow(10,aux);
  return (VCC / (1 + (aux/R0_value)));
}
